CC      := gcc
TARGET  ?= $(notdir $(shell pwd))
CFLAGS  += -Wall -Wextra -std=c99
LDFLAGS +=

SRCS ?=
ASMS ?=
OBJS := $(SRCS:.c=.o) $(ASMS:.s=.o)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $<

clean::
	rm -f $(OBJS)

nuke:: clean
	rm -f $(TARGET)

print_target:
	@echo $(TARGET)

print_objs:
	@echo $(OBJS)
