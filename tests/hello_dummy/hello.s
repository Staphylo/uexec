
.text
.global _start
_start:
    xor %rax, %rax
    inc %rax
    xor %rdi, %rdi
    mov $msg, %rsi
    mov $16, %rdx
    syscall
    mov $60, %rax
    ;xor %rdi, %rdi
    mov $42, %rdi
    syscall

.data
msg:
    .ascii "Hello World !!!\n"
