#ifndef LOG_H_
# define LOG_H_

# include <stdio.h>

# define XSTR(S) STR(S)
# define STR(S) #S

# define LOG_ERROR  ("error",  10)
# define LOG_WARN   ("warn",   20)
# define LOG_NOTICE ("notice", 30)
# define LOG_DEBUG  ("debug",  40)

# define LOG_TYPE_NAME(Type) LOG_TYPE_EXTRACT_NAME Type
# define LOG_TYPE_EXTRACT_NAME(Name, Level) Name

# define LOG_TYPE_LEVEL(Type) LOG_TYPE_EXTRACT_LEVEL Type
# define LOG_TYPE_EXTRACT_LEVEL(Name, Level) Level

# define log(Type, Fmt, ...) \
    if (DEBUG_LEVEL >= LOG_TYPE_LEVEL(Type)) \
        fprintf(stderr, "[" __FILE__ ":" XSTR(__LINE__) "] " \
                        LOG_TYPE_NAME(Type) ": " Fmt "\n", ## __VA_ARGS__)

# ifndef DEBUG_LEVEL
#  define DEBUG_LEVEL 40
# endif

#endif /* LOG_H_ */

