#ifndef LOADER_H_
# define LOADER_H_

# include <stdbool.h>

struct binary_context;

struct binary_loader {
    const char *name;
    // test if the given binary matches the current architecture
    bool (*test)(struct binary_context *bin);
    // init the context of the architecture in the binary_context structure
    bool (*init)(struct binary_context *bin);
    // load the binary into the ram
    bool (*load)(struct binary_context *bin);
    // setup the stack and the execution environment
    bool (*exec)(struct binary_context *bin);
    // free everything allocated correctly
    void (*exit)(struct binary_context *bin);
};

# define BINARY_LOADERS __table(struct binary_loader, "binary_loaders")
# define __binary_loader __table_entry(BINARY_LOADERS, 42)

#endif /* LOADER_H_ */

