#ifndef BINARIES_H_
# define BINARIES_H_

# include "table.h"

struct binary_entry {
    const char *name;
    void *start;
    void *end;
};

# define BINARY_ENTRIES __table(struct binary_entry, "binary_entries")
# define __binary_entry __table_entry(BINARY_ENTRIES, 01)

void binaries_dump();

const struct binary_entry * binary_get(const char *name);

#endif /* BINARIES_H_ */

