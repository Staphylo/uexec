#include <string.h>

#include "binaries.h"
#include "log.h"

void
binaries_dump()
{
    struct binary_entry *entry;
    log(LOG_DEBUG, "binary list:");
    foreach_table_entry(entry, BINARY_ENTRIES) {
        log(LOG_DEBUG, " - %s : %p <-> %p",
                       entry->name, entry->start, entry->end);
    }
}

const struct binary_entry *
binary_get(const char *name)
{
    struct binary_entry *entry;

    foreach_table_entry(entry, BINARY_ENTRIES)
        if (!strcmp(entry->name, name))
            return entry;

    return NULL;
}
