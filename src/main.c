#include <stdio.h>
#include <libgen.h>

#include "log.h"
#include "types.h"
#include "unpack.h"
#include "binaries.h"

static void
usage(const char *prgname)
{
    struct binary_entry *entry;
    struct binary_loader *loader;

    printf("usage: %s [OPTS]\n", prgname);
    printf("usage: binary ...\n");

    printf("embeded binaries are:\n");
    foreach_table_entry(entry, BINARY_ENTRIES) {
        printf(" - %s\n", entry->name);
    }

    printf("embeded loaders are:\n");
    foreach_table_entry(loader, BINARY_LOADERS) {
        printf(" - %s\n", loader->name);
    }
}

int
main(int argc, char *argv[], char *envp[])
{
    const char *name = basename(argv[0]);
    const struct binary_entry *entry = binary_get(name);
    struct binary_context *ctx;

    if (!entry) {
        usage(argv[0]);
        return 1;
    }

    log(LOG_DEBUG, "Init...");
    ctx = binary_init(entry);
    if (!ctx) {
        log(LOG_ERROR, "Initialsation failed");
        return 1;
    }

    ctx->argc = argc;
    ctx->argv = argv;
    ctx->envp = envp;

    log(LOG_DEBUG, "Unpacking...");
    if (!binary_load(ctx)) {
        log(LOG_ERROR, "Unkpacking failure");
        return 1;
    }

    log(LOG_DEBUG, "Execing...");
    if (!binary_exec(ctx)) {
        log(LOG_ERROR, "Exec failure");
        return 1;
    }

    // will never return if everything went well...
}
