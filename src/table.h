#ifndef TABLE_H_
# define TABLE_H_

# define __packed __attribute__((packed))
# define __unused __attribute__((unused))
# define __section(Section) __attribute__((__section__(Section)))
# define __aligned(Alignment) __attribute__((__aligned__(Alignment)))

// create table type
# define __table(Type, Name) (Type, Name)

// create a table entry
# define __table_entry(Table, Index)              \
         __section(__table_section(Table, Index)) \
         __aligned(__table_alignment(Table))

// get type
# define __table_type(Table) __table_type_sub Table
# define __table_type_sub(Type, Name) Type

// get name
# define __table_name(Table) __table_name_sub Table
# define __table_name_sub(Type, Name) Name

// get section name
# define __table_str(Str) #Str
# define __table_section(Table, Index) \
    ".tbl." __table_name(Table) "." __table_str(Index)

// get alignment
# define __table_alignment(Table) __alignof__(__table_type(Table))

# define __table_entries(Table, Index)                 \
    (<% static __table_type(Table) __table_entries[0]  \
               __table_entry(Table, Index) __unused; \
    __table_entries; %>)


// iterators
# define table_start(Table) __table_entries(Table, 00)
# define table_end(Table) __table_entries(Table, 99)

// iterate over the table
# define foreach_table_entry(Ptr, Table) \
    for (Ptr = table_start(Table); \
         Ptr < table_end(Table); \
         ++Ptr)

#endif /* TABLE_H_ */

