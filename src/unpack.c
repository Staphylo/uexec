#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "unpack.h"
#include "loader.h"
#include "log.h"


struct binary_context *binary_init(const struct binary_entry *entry)
{
    const struct binary_loader *loader;
    struct binary_context *ctx;

    ctx = malloc(sizeof (struct binary_context));
    ctx->entry = entry;
    ctx->loader = NULL;

    foreach_table_entry(loader, BINARY_LOADERS) {
        log(LOG_DEBUG, "trying loader %s", loader->name);
        if (loader->test(ctx)) {
            ctx->loader = loader;
            log(LOG_DEBUG, "using loader %s", loader->name);
            break;
        }
    }

    if (!ctx->loader) {
        log(LOG_ERROR, "Failed to find a correct loader for this binary");
        goto cleanup;
    }


    log(LOG_DEBUG, "Calling architecture init");
    if (!ctx->loader->init(ctx)) {
        log(LOG_ERROR, "Failed to init architecture loader");
        goto cleanup;
    }

    return ctx;

cleanup:
    free(ctx);
    return NULL;
}

bool
binary_load(struct binary_context *bin)
{
    log(LOG_DEBUG, "Calling architecture load");
    if (!bin->loader->load(bin)) {
        log(LOG_DEBUG, "Error while running architecture load");
        return false;
    }

    return true;
}

bool
binary_exec(struct binary_context *bin)
{
    log(LOG_DEBUG, "Calling architecture exec");
    if (!bin->loader->exec(bin)) {
        log(LOG_DEBUG, "Error while running architecture exec");
        return false;
    }

    return true;
}

void binary_free(struct binary_context *bin)
{
    if (bin)
    {
        if (bin->ctx)
            bin->loader->exit(bin);
        free(bin);
    }
}
