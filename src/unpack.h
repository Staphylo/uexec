#ifndef UNPACK_H_
# define UNPACK_H_

# include <elf.h>

# include "loader.h"
# include "types.h"
# include "binaries.h"

struct binary_context {
    const struct binary_entry *entry;
    const struct binary_loader *loader;
    void *ctx;
    char **argv;
    char **envp;
    int argc;
};

struct binary_context *binary_init(const struct binary_entry *entry);

bool binary_load(struct binary_context *bin);

bool binary_exec(struct binary_context *bin);

void binary_free(struct binary_context *bin);

#endif /* UNPACK_H_ */
