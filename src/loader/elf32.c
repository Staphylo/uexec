#include "../loader.h"
#include "../unpack.h"

static bool binary_elf32_false(struct binary_context *bin)
{
    (void)bin;
    return false;
}

static void binary_elf32_void(struct binary_context *bin)
{
    (void)bin;
}

struct binary_loader binary_elf32 __binary_loader = {
    .name = "elf32",
    .test = binary_elf32_false,
    .init = binary_elf32_false,
    .load = binary_elf32_false,
    .exec = binary_elf32_false,
    .exit = binary_elf32_void,
};
