#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <elf.h>

#include "../loader.h"
#include "../unpack.h"
#include "../log.h"

struct elf64_ctx {
    Elf64_Ehdr *ehdr;
    size_t size;
};

// positive offset
#define POFF(Addr, Off) (((u8*)(Addr)) + Off)

// page alignement macro
#define PSIZE 0x1000
#define PALIGN(Value) ((((Value) + PSIZE) >> 12) << 12)

static bool
binary_elf64_load_segment(struct binary_context *bin, Elf64_Phdr *phdr)
{
    size_t psize = PALIGN(phdr->p_memsz);
    size_t page_offset = phdr->p_vaddr & (PSIZE - 1);
    void  *paddr = (void *)phdr->p_vaddr;

    void *page_addr = (void*)(phdr->p_vaddr - page_offset);

    log(LOG_DEBUG, "Segment to load [0x%lx - 0x%lx]",
        phdr->p_vaddr, phdr->p_vaddr + phdr->p_memsz);

    if (!psize) {
        log(LOG_WARN, "Empty segment");
        return true;
    }

    log(LOG_DEBUG, "Sement size %lx -> %lx", phdr->p_memsz, psize);
    log(LOG_DEBUG, "Memory @ %p [ %zu | 0x%lx ]", paddr, psize, psize);

    page_addr = mmap(page_addr, psize, PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, -1, 0);
    if (page_addr == MAP_FAILED) {
        log(LOG_ERROR, "Segment loading failed (mmap: (%s))", strerror(errno));
        return false;
    }

    memcpy(paddr, POFF(bin->entry->start, phdr->p_offset), phdr->p_filesz);
    size_t remaining = phdr->p_memsz - phdr->p_filesz;
    if (remaining)
        memset(POFF(paddr, phdr->p_filesz), 0, remaining);

    int prot = 0;
    if (phdr->p_flags & PF_R)
        prot |= PROT_READ;
    if (phdr->p_flags & PF_W)
        prot |= PROT_WRITE;
    if (phdr->p_flags & PF_X)
        prot |= PROT_EXEC;

    if (mprotect(page_addr, psize, prot) == -1) {
        log(LOG_ERROR, "Segment protection failed (mprotect: %s)",
                       strerror(errno));
        return false;
    }

    return true;
}

static bool binary_elf64_test(struct binary_context *bin)
{
    Elf64_Ehdr *ehdr = (Elf64_Ehdr *)bin->entry->start;
    if (memcmp(ehdr->e_ident, ELFMAG, SELFMAG))
    {
        log(LOG_DEBUG, "File is not an elf");
        return false;
    }

    if (ehdr->e_ident[EI_CLASS] != ELFCLASS64)
    {
        log(LOG_ERROR, "File is not an elf64");
        return false;
    }

    return true;
}

static bool binary_elf64_init(struct binary_context *bin)
{
    struct elf64_ctx *ctx = malloc(sizeof(struct elf64_ctx));
    if (!ctx)
        return false;

    ctx->ehdr = (Elf64_Ehdr *)bin->entry->start;
    ctx->size = bin->entry->end - bin->entry->start;

    bin->ctx = ctx;

    return true;
}

static bool binary_elf64_load_interp(struct binary_context *bin, Elf64_Phdr *interp)
{
    (void)bin;
    (void)interp;

    log(LOG_DEBUG, "TODO: interp");

    return true;
}

static bool binary_elf64_load(struct binary_context *bin)
{
    struct elf64_ctx *ctx = bin->ctx;

    if (ctx->size < sizeof (Elf64_Ehdr))
        return false;

    Elf64_Phdr *phdr = (Elf64_Phdr *)(POFF(bin->entry->start, ctx->ehdr->e_phoff));
    for (Elf64_Half i = 0; i < ctx->ehdr->e_phnum; ++i)
    {
        if (phdr[i].p_type == PT_LOAD)
        {
            if(!binary_elf64_load_segment(bin, &phdr[i]))
                return false;
        }
        else if (phdr[i].p_type == PT_INTERP)
        {
            log(LOG_DEBUG, ".interp section found");
            if (!binary_elf64_load_interp(bin, &phdr[i]))
                return false;
        }
    }

    return true;
}

static bool binary_elf64_exec(struct binary_context *bin)
{
    struct elf64_ctx *ctx = bin->ctx;

    // setup stack and stuff

    __asm__ __volatile__ (
        "jmp *%0"
        :
        : "m" (ctx->ehdr->e_entry)
    );

    return true;
}

static void binary_elf64_exit(struct binary_context *bin)
{
    free(bin->ctx);
}

struct binary_loader binary_elf64 __binary_loader = {
    .name = "elf64",
    .test = binary_elf64_test,
    .init = binary_elf64_init,
    .load = binary_elf64_load,
    .exec = binary_elf64_exec,
    .exit = binary_elf64_exit,
};
