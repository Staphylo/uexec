# Tools

CC    = gcc
LD    = ld
MKDIR = mkdir
ECHO  = echo -e
RM    = rm

# Flags

CFLAGS  := -Wall -Wextra -std=c11
CFLAGS  += -g -ggdb3 -gdwarf-2 -fPIC
LDFLAGS := -Wl,-dT,scripts/add.lds -pie #-Wl,-verbose
LDFLAGS += -Wl,-Ttext,0x100000 -Wl,-Tdata,0x200000 -Wl,-Tbss,0x300000

# Build data

TARGET  := uexec

BUILDDIR := build

SRCDIRS  :=
SRCDIRS  += src

BINDIRS  := binaries

BINS     = $(wildcard $(BINDIRS)/*)
#BINS     += binaries/hello/hello binaries/hello_dyn/hello_dyn

MAKEDEPS := Makefile utils/table_entry.sh
# rulez and computations

#SRCS     =
#DEPS     =
#BINS     =

all: $(TARGET)

ifeq ($(wildcard $(BUILDDIR)),)
    $(shell $(MKDIR) -p $(BUILDDIR))
endif

ifeq ($(wildcard $(BINDIRS)),)
    $(shell $(MKDIR) -p $(BINDIRS))
endif

define make_call
$(call $(1),$(2),$(basename $(notdir $(2))))
endef

###############################################################################
# Binary to object function and loading
###############################################################################

# ld -r -b binary -o data.o data.txt
# objcopy -I binary -O elf32-i386 --binary-architecture i386 data.txt data.o

define object_bins
$(call object_bins_sub,$(1),$(notdir $(1)))
endef

# no dependencies on generated file
define object_bins_sub
$$(BUILDDIR)/bins/$(2).o: $(1) $(MAKEDEPS)
	@$(ECHO) "\e[34mBinary blob \e[0m=> \e[32m'$(2)'\e[0m"
	@$$(LD) -r -b binary -o $$@ $$<
$$(BUILDDIR)/bins/$(2).tbl.o: $(BUILDDIR)/bins/$(2).o $(MAKEDEPS)
	@$(ECHO) "\e[34mBinary table \e[0m=> \e[32m'$(2)'\e[0m"
	@$$(shell utils/table_entry.sh $(1) > $$(@:.o=.c))
	@$$(CC) $$(CFLAGS) -I src/ -c -o $$@ $$(@:.o=.c)

BINOBJS += $$(BUILDDIR)/bins/$(2).o
BINTBLS += $$(BUILDDIR)/bins/$(2).tbl.o
endef

# loading binaries preferences
ifneq ($(BINDIRS),)
    $(foreach bin,$(BINS),$(eval $(call object_bins,$(bin))))
endif

ifeq ($(wildcard $(BUILDDIR)/bins),)
    $(shell $(MKDIR) -p $(BUILDDIR)/bins)
endif


###############################################################################
# Making dependencies of file to be compiled
###############################################################################

define object_deps
	@$(ECHO) "\e[34mDependencies \e[0m=> \e[32m'$(1)'\e[0m"
	@$(MKDIR) -p $(BUILDDIR)/deps/$(dir $(1))
	@$(CC) $(CFLAGS) -M $(1) -MG -MP | \
	sed 's/\.o\s*:/_DEPS +=/' > $(BUILDDIR)/deps/$(1).d
endef

$(BUILDDIR)/deps/%.d: %
	$(call make_call,object_deps,$<)

###############################################################################
# Generating compile rules and object list
###############################################################################

define object_rules
$$(BUILDDIR)/$(2).o: $(1) $$($(2)_DEPS) $$(MAKEDEPS)
	@$(ECHO) "\e[34mCompiling \e[0m=> \e[32m'$(1)'\e[0m"
	@$$(CC) $$(CFLAGS) -c -o $$@ $$<
OBJS += $$(BUILDDIR)/$(2).o
$$(BUILDDIR)/deps/$(1).d: $$($(2)_DEPS)
endef

clean::
	@$(ECHO) "\e[01;34mCleaning object files\e[0m"
	@rm -f $(OBJS)

nuke::
	@$(ECHO) "\e[01;34mPurging compilation files\e[0m"
	@rm -rf $(BUILDDIR)

print-%:
	@$(ECHO) $($*)

print_srcdirs:
	@$(ECHO) $(SRCDIRS)

include config.mk
SRCS += $(wildcard $(patsubst %,%/*.c,$(SRCDIRS)))
SRCS += $(foreach loader,$(LOADERS),$(patsubst %,%/loader/$(loader).c,$(SRCDIRS)))
print_srcs:
	@$(ECHO) $(SRCS)

print_bindirs:
	@$(ECHO) $(BINDIRS)

print_binobjs:
	@$(ECHO) $(BINOBJS)

print_bins:
	@$(ECHO) $(BINS)

print_objs:
	@$(ECHO) $(OBJS)

DEPS = $(patsubst %,$(BUILDDIR)/deps/%.d,$(SRCS))
print_deps:
	@$(ECHO) $(DEPS)

ifneq ($(DEPS),)
    -include $(DEPS)
endif

ifneq ($(SRCS),)
    $(foreach src,$(SRCS),$(eval $(call make_call,object_rules,$(src))))
endif

$(TARGET): $(BINOBJS) $(BINTBLS) $(OBJS) $(MAKEDEPS)
	@$(ECHO) "\e[34mLinking \e[0m=> \e[32m'$@'\e[0m"
	@$(CC) $(LDFLAGS) -o $(TARGET) $(OBJS) $(BINOBJS) $(BINTBLS)


testpre:
	@for folder in tests/*/; do \
	    echo -e "\e[34mMaking test \e[0m=> \e[32m'$$folder'\e[0m"; \
	    make -sC $$folder all; \
	    cp $$folder/`basename $$folder` binaries/; \
	done

testpost:
	@for bin in binaries/*; do \
	    bin=`basename $$bin`; \
	    echo -e "\e[34mSymlink \e[32m'$$bin'\e[0m => \e[32m'$(TARGET)'\e[0m"; \
	    [ -h $$bin ] && rm -f $$bin; \
	    ln -s uexec $$bin; \
	done

tests:: testpre $(TARGET) testpost

.PHONY: clean binaries all
.SUFFIXES:
