Userland Exec
=============

What does it do
---------------

The goal of this project is to embed on or more binaries in one binary and to
be able to run one of them without using execve(2) thus storing the binary file
in the file system.

There are 3 parts in this projects
 - packing the binaries in one binary which is the loader
 - loading the binary into ram (maybe a layer of unpacking will be added)
 - executing the binary


Run
---

To run this software you have to simply move the binary you want to embed in
the binaries folder and then make the project.

    make # in order to create binaries folder
    cp /bin/true binaries/
    make
    ln -s uexec ./true
    ./true

Test
----

The project is still under development and is for now only capable of running
hello_dummy binary.

    make tests

Author
------

Samuel "Staphylo" Angebault <staphyloa@gmail.com>

Thanks
------

To iPXE source code for creating the wonderful linker tables and also for their
proper build system.
