#!/bin/sh

path=$1
path=${path%%.tbl.o}
filename=$(basename "$path")
sym=${path//./_}
sym=_binary_${sym//\//_}

cat << EOF
#include "binaries.h"

extern void *${sym}_start[];
extern void *${sym}_end[];

struct binary_entry ${filename}_entry __binary_entry = {
    .name  = "${filename}",
    .start = ${sym}_start,
    .end   = ${sym}_end
};

EOF
